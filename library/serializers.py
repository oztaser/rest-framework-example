from library.models import Books
from rest_framework import serializers

class BookSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Books
		fields = ('name', 'writer')
