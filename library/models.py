from django.db import models

class Books(models.Model):
	name       = models.CharField(max_length=80)
	summary    = models.TextField()
	writer     = models.CharField(max_length=100)
