from library.serializers import BookSerializer
from library.models import Books

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status


class BookList(views.APIView):
	def get(self, request, format=None):
		books = Books.objects.all()
		serializer = BookSerializer(books, many=True)
		return Response(serializer.data)
