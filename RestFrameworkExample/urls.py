from django.conf.urls import patterns, url, include
from rest_framework.urlpatterns import format_suffix_patterns
from library import views
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^book/$', views.BookList.as_view()),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns = format_suffix_patterns(urlpatterns)
